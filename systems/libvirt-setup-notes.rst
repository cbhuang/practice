==============================================
libvirt (kvm-qemu) + virt-manager setup note
==============================================

**CAUTION**: Heavily experimental! No warranty! Backup before anything!

Got my Acer Aspire 5 A515-51G-55SC laptop (Intel i5-8250U + nvidia GeForce
MX130) running kernal-based virtual machines (KVMs)!! ``libvirt`` and
``virt-manager`` is used. This note is for future reproduction.

Goods:

* The minimal process (all) now becomes super-easy as of 2019.2.
* Network is bridged automagically by ``virt-manager``!
    * Needn't mess around with bridging using NetworkManager!
    * Also works for wireless network!

Notice:

* GPU seems to be passed through successfully, but OpenGL is not working in
  the guest system.
* When a guest reboot is required, must shutdown instead. Otherwise may cause
  login loops. Cause unknown.
* Use ``virsh`` for editing any xml file because it checks conformance.


Paths
=====================================================================

* Main libvirt settings: ``/etc/libvirt/``
  + VM instances: ``/etc/libvirt/qemu/``
  + Shared storage: ``/etc/libvirt/storage/``

* Default .qcow2 storage path: ``/var/lib/libvirt/images/``
  + Change: ``virsh pool-edit default``
  + **Don't** edit ``/etc/libvirt/storage/default.xml`` directly!


1. Install Packages
=====================================================================

* Packages
    * Debian: start from deb-inst-1_ or deb-inst-2_
    * Ubuntu: ubuntu-inst_
    * CentOS: rhel-inst_
    * Some additional development packages may be required.
* ``sudo systemctl enable libvirtd``
* Reboot.


2. Create Guest VM - Basic Setup
=====================================================================

1. Download ``debian-x.x.x-amd64-netinst.iso``

2. Open Virt-Manager and create new machine
    * Suggested minimal space: 10G+768M swap
        * Debian 9 eats up 4.3G itself
        * 5.6G is used when all is set
    * **Let the virtual bridge to be created automatically**
    * Customize configuration before install
        * **Copy host CPU config**
        * Video model = ``QXL``
    * Set qemu network to start on boot.

3. Host side - basic tweaks
    * ``sudo virsh edit <vm_name>``
    * set ``vgamem`` to 128 or 256M in ``<video>``
    * (optional) Move VM file out of default path: ``<source file='....' />``
        - Original permissions: ``0600 root:root``
        - **NTFS**: ``0666 bill:bill`` because unable to set user ``libvirt-qemu``
    * After change: ``sudo systemctl restart libvirtd``

4. Guest side - basic tweaks
    + Clipboard: (`AskUbuntu <clipboard_>`_)

      .. code-block:: bash

        $ sudo apt install spice-vdagent
        # There are ways to restart X, but somehow not promising in a VM
        $ sudo reboot now


5. Kernal Mode Setting
  * NOTE: This is somehow not working independently, but seems to work after
    GPU passthrough!
  * Add ``/etc/X11/xorg.conf.d/kms.conf`` in **guest system**:
    - Note: Open *KInfoCenter* to confirm the PCI BusID of graphic card. But
      this seems to be optional.

    ::

      # (optional section for OpenGL on debian 9 guest, may not work)
      Section "Files"
          ModulePath "/usr/lib/nvidia/current"
          ModulePath "/usr/lib/xorg/modules"
      EndSection

      Section "Monitor"
              Identifier "Monitor0"
              HorizSync   30 - 80
              VertRefresh 40 - 90
      EndSection

      Section "Device"
              Identifier "Card0"
              Driver     "qxl"
              # Not sure if this can be omitted.
              BusID      "PCI:0:2:0"
      Endsection

      Section "Screen"
              Identifier "Screen0"
              Device     "Card0"
              Monitor    "Monitor0"
              SubSection "Display"
                      Viewport 0 0
                      Modes "1920x1080"
              EndSubSection
      EndSection

  * Check how it works: search ``/var/log/Xorg.0.log`` in the guest.
      * Search for ``modesetting``
      * Fail message: ``(EE)`` AIGLX: reverting to software rendering


Now the VM should be usable as a simple server. However, the graphics are too
slow for desktop applications unless we do a GPU passthrough.


3. GPU Passthrough
=====================================================================

`Main referenced tutorial: <gpu-main_>`_. Require some trial and error!

`An authoritative reference (archlinux): <archlinux-pcipass_>`_


Prerequisites
------------------------

`Check <gpu-main_>`_ if ``bumblebee`` has been set up for hosts having Intel
CPU + Nvidia GPU:

.. code-block:: bash

  cat /proc/acpi/bbswitch
    # should give ``0000:01:00.0 OFF``
  optirun cat /proc/acpi/bbswitch
    # 0000:01:00.0 ON
  nvidia-smi
    # wouldn't communicate with nvidia driver
  optirun nvidia-msi
    # should give a table


1. Host Graphics Info
---------------------------

Example:

.. code-block:: bash

   $ lspci -vnn

::

   # intel (not to be passed)
   00:02.0 VGA compatible controller [0300]: Intel Corporation Device [8086:5917] (rev 07) (prog-if 00 [VGA controller])
           Subsystem: Acer Incorporated [ALI] Device [1025:1193]
           .......
           Kernel driver in use: i915
           Kernel modules: i915

   # geforce MX-130 (to be passed)
   01:00.0 3D controller [0302]: NVIDIA Corporation Device [10de:174d] (rev ff) (prog-if ff)
           !!! Unknown header type 7f
           Kernel modules: nvidia

* vendor-id:device-id = **10de:174d**
* PCI slot = **01:00.0** (i.e. `0x0000:01:00.0 <https://wiki.xen.org/wiki/Bus:Device.Function_(BDF)_Notation>`_)


2. Host Setup
---------------------------

1. Edit ``/etc/default/grub``:

  * ``GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on"``
  * (not sure, not chosen) `intel_iommu=on,igfx_off <igfx-off_>`_ turns off
    the VM's direct access to integrated graphics. Add if it bugged.
  * ``sudo update-grub``
  * `Check iommu is working <vfio-blog_>`_ after reboot:
    ``find /sys/kernel/iommu_groups/ -type l``

2. Load `VFIO module <mkinitcpio_>`_ into linux kernel (optional?)

  * Append to ``/etc/initramfs-tools/modules``:
    Order by `archlinux docs <initramfs-order_>`_ over `main ref <gpu-main_>`_
    ::

      vfio_pci
      vfio
      vfio_iommu_type1
      vfio_virqfd
      vhost-net

  * Create ``/etc/modprobe.d/vfio.conf``:
    ::

      options vfio-pci ids=10de:174d

  * ``sudo update-initramfs -u`` and reboot later

4. `Edit <libvirt-xml_>`_ xml file of the VM (``virsh edit <vm_name>``):

  * Add ``<hostdev>`` after ``<video>``: set PCI slots for host and guest

  .. code-block:: xml

    <hostdev mode='subsystem' type='pci' managed='yes'>
      <source>
        <address domain='0x0000' bus='0x01' slot='0x00' function='0x0'/>
      </source>
      <address type='pci' domain='0x0000' bus='0x01' slot='0x01' function='0x0' multifunction='on'/>
    </hostdev>

  * Add ``<qemu:commandline>`` after ``<device>``: set host GPU type

  .. code-block:: xml

    # beginning
    <domain type='kvm' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
    # back
    <qemu:commandline>
    <qemu:arg value='-set'/>
    <qemu:arg value='device.hostdev0.x-pci-sub-vendor-id=0x10de'/>
    <qemu:arg value='-set'/>
    <qemu:arg value='device.hostdev0.x-pci-sub-device-id=0x174d'/>
    </qemu:commandline>

  * Edit `(critical!) <cpu-pass_>`_: ``<cpu mode='host-passthrough'>``


3. Guest Setup
---------------------------

* Edit ``/etc/apt/sources.list``
  - Add channel: stretch-backports
  - Add: main contrib non-free

::

  deb http://ftp.nl.debian.org/debian/ stretch main contrib non-free
  deb-src http://ftp.nl.debian.org/debian/ stretch main contrib non-free

  deb http://security.debian.org/debian-security stretch/updates main contrib non-free
  deb-src http://security.debian.org/debian-security stretch/updates main contrib non-free

  # stretch-updates, previously known as 'volatile'
  deb http://ftp.nl.debian.org/debian/ stretch-updates main contrib non-free
  deb-src http://ftp.nl.debian.org/debian/ stretch-updates main contrib non-free

  # for nvidia 390.xx
  deb http://deb.debian.org/debian stretch-backports main contrib non-free

* Install and reboot

.. code-block:: bash

  # Can install in X window
  dpkg --add-architecture i386
  apt update
  apt install -t stretch-backports nvidia-driver
  reboot now

* **DON'T** install ``bumblebee-nvidia`` in the client side!
  * ``/var/log/Xorg.0.log`` may have some ``(EE)`` (glx extension missing).
  * ``glxinfo`` also gives error.



Miscellaneous Works After Setup
=====================================================================

* **Backup the VM and the xml settings file!!**
* `Setup Samba server <samba_>`_ for file sharing
* ``apt remove libreoffice* ``
* ``apt autoremove``, ``apt clean``
* Add ``HideUsers=libvirt-qemu`` in ``/etc/sddm.conf`` to hide "Libvirt Qemu"
  from the login screen.
* (TODO) Enable OpenGL in guest OS.
    * Can `pci-stub approach <pci-stub_>`_ work? (not verified)
* (TODO) Restrict permissions of .qcow2 on NTFS file system


Unsure
=====================================================================

Pre- and Post-run Scripts
----------------------------------

In my machine this is not required as ``/sys/bus/pci/drivers/vfio-pci``
will be automatically created by libvirt before each VM boot. Probably the
xml settings is really that smart...

.. code-block:: bash
    ### pre-boot script of VM run ###
    # optional, if the directory does not exist!
    modprobe vfio-pci disable_vga=1

    echo "10de 174d" > "/sys/bus/pci/drivers/vfio-pci/new_id"

    ### post-shutdown script ###
    # Unbind dGPU from vfio-pci driver
    echo "0000:01:00.0" > "/sys/bus/pci/drivers/vfio-pci/0000:01:00.0/driver/unbind"
    # Power off your dGPU
    echo "OFF" >> /proc/acpi/bbswitch
    # check
    optirun nvidia-smi


`Pass OVMF bootloader <libvirt-xml_>`_
------------------------------------------

* Edit ``/etc/libvirt/qemu.conf`` to pass OVMF (UEFI firmware):
  ::

     # don't pass others!
     nvram = ["/usr/share/OVMF/OVMF_CODE.fd:/usr/share/OVMF/OVMF_VARS.fd"]

* Edit guest VM xml settings:
  .. code-block:: xml

    <os>
      <type arch='x86_64' machine='pc-q35-2.10'>hvm</type>
      <loader readonly='yes' type='pflash'>/usr/share/OVMF/OVMF_CODE.fd</loader>
      <nvram>/usr/share/OVMF/OVMF_VARS.fd</nvram>
    </os>


Others
------------------------------
* uml-utilities <deb-inst-3_>_ ?


.. References
.. _deb-inst-1: https://www.server-world.info/en/note?os=Debian_8&p=kvm
.. _deb-inst-2: https://www.linux.com/learn/create-and-run-virtual-machines-virt-manager
.. _deb-inst-3: https://arrayfire.com/using-gpus-kvm-virutal-machines
.. _ubuntu-inst: https://wiki.libvirt.org/page/UbuntuKVMWalkthrough
.. _rhel-inst: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-installing_the_virtualization_packages-installing_virtualization_packages_on_an_existing_red_hat_enterprise_linux_system
.. _gpu-main: https://gist.github.com/Misairu-G/616f7b2756c488148b7309addc940b28
.. _clipboard: https://askubuntu.com/questions/858649
.. _igfx-off: https://stackoverflow.com/questions/22867061
.. _libvirt-xml: https://gist.github.com/anonymous/500f1edf89d6f22c40bd2cbbdec6490b
.. _cpu-pass: https://stackoverflow.com/questions/46219552
.. _samba: https://wiki.debian.org/SambaServerSimple
.. _archlinux-pcipass: https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF#With_vfio-pci_loaded_as_a_module
.. _mkinitcpio: https://unix.stackexchange.com/questions/268903
.. _initramfs-order: https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF#With_vfio-pci_loaded_as_a_module
.. _pci-stub: https://ycnrg.org/vga-passthrough-with-ovmf-vfio
.. _vfio-blog: http://vfio.blogspot.com/2015/05/vfio-gpu-how-to-series-part-3-host.html
