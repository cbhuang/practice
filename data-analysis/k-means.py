
from sklearn.cluster import KMeans
import numpy as np
import numpy.random as rnd

data = np.zeros((300, 2))  # k-means eats n*2
# first group of data
data[:100, 0] = rnd.standard_normal(100)
data[:100, 1] = rnd.standard_normal(100)
# 2nd gp
data[100:200, 0] = 3 + 0.5 * rnd.standard_normal(100)
data[100:200, 1] = 3 + rnd.standard_normal(100)
# 3rd gp
data[200:300, 0] = -3 + 0.5 * rnd.standard_normal(100)
data[200:300, 1] = -3 + rnd.standard_normal(100)

# classify data itself
cat_pred = KMeans(n_clusters=3).fit_predict(data)

# visualize
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(8,6),dpi=100)
ax = fig.add_subplot(1,1,1)
ax.scatter(data[:, 0], data[:, 1], c=cat_pred)  # (x, y, c=category)
fig.show()

print("Press any key to end.")
input()
plt.close(fig)
